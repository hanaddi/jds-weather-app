import { StyleSheet, Dimensions, StatusBar } from 'react-native'; 
// import { useFonts } from 'expo-font';
// import { Font } from 'expo';
import React, {useRef} from 'react';
import { View, Text as NativeText, TextInput, Image, TouchableOpacity, Modal, ScrollView, ImageBackground, TouchableWithoutFeedback, FlatList } from 'react-native';

import { MaterialCommunityIcons, Ionicons, MaterialIcons } from '@expo/vector-icons';

const SCREEN_HEIGHT = Dimensions.get('screen').height;
const SCREEN_WIDTH = Dimensions.get('screen').width;
const WINDOW_HEIGHT = Dimensions.get('window').height;
const WINDOW_WIDTH = Dimensions.get('window').width;
const STATUS_BAR_HEIGHT = StatusBar.currentHeight || 30; 
export const BACKGROUND_IMAGE = require('./assets/images/pexels-aron-visuals-1643113.jpg');

export const El = {
	text:({children, style, ...param})=>(
		<NativeText {...param} style={[Style.defaultText, style]}>{children}</NativeText>
	),
	statsIcon:({icon, title, value, show})=>(
		show?
		(<View style={Style.statsContainer}>
			<Text style={Style.center}>{icon}</Text>
			<Text style={Style.center}>{value}</Text>
			<Text style={Style.center}>{title}</Text>
		</View>)
		:
		(<View style={[Style.statsContainer, {opacity:0}]}>
			<Text style={Style.center}> </Text>
			<Text style={Style.center}><Ionicons name="hourglass-outline" size={32} /></Text>
			<Text style={Style.center}> </Text>
		</View>)
	),
	cell:({icon, time, value, show})=>(
		show?
		(<TouchableOpacity>
		<View style={Style.cellContainer}>
			<Text style={Style.center}>{time}</Text>
			<ImageBackground source={{uri:`https://openweathermap.org/img/w/${icon}.png`}} style={Style.iconMedium} resizeMode='contain'>
			</ImageBackground>
			<Text style={Style.center}>{value}</Text>
		</View>
		</TouchableOpacity>)
		:
		(<View style={Style.cellContainer}>
			<Text style={Style.center}> </Text>
			<ImageBackground source={{uri: "data:image/png;base64, wolQTkcNChoKAAAADUlIRFIAAAABAAAAAQgGAAAAHxXDhMKJAAAACklEQVR4AWMAAQAABQABNsOQwojDnQAAAABJRU5Ewq5CYMKC"}} style={Style.iconMedium} resizeMode='contain'>
				<Text style={[Style.center, {opacity: 0.3}]}><MaterialCommunityIcons name="folder-outline" size={34}  /></Text>
			</ImageBackground>
			<Text style={Style.center}> </Text>
		</View>)
	),
	cellDate:({day, date})=>(
		<View style={[Style.statsContainer, Style.cellDate]}>
			<Text style={[Style.center, Style.title2]}>{day}</Text>
			<Text style={[Style.center, Style.title2]}>{date}</Text>
		</View>
	),

	button :({text='', ...param})=>{
		text = (text || '').trim();
		return(
			<TouchableOpacity {...param} style={Style.button}>
				<Text style={Style.title2}>{text}</Text>
			</TouchableOpacity>
		);
	},

	error :({text=''})=>{
		text = (text || '').trim();
		return text && text.length>0?(
			<View style={Style.errorText}>
				<MaterialIcons name="error" size={24} color={color.red} style={{marginRight:10}} />
				<Text style={{flex: 1, color:color.red}} >{text}</Text>
			</View>
		):(<></>);
	},
	space: ()=>(
		<View style={Style.iconMedium}></View>
	)
};

const Text = El.text;
const color = {
	gray : 'rgba(50,50,70,0.4)',
	white: '#fff',
	grey : '#346',
	mistyBlue: '#adb3bc',

	error: '#FFBBBB',
	red: '#B23431',
};
export const xColor = color;
const fontTitle = 'Rowdies-Bold';
const defaultFontFamily = 'Roboto';
const defaultFontSize = 14;

const Style = StyleSheet.create({
	centeredView: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
	},
	outerContainer:{
		flex:1,
		backgroundColor: color.gray,
	},
	appContainer: {
		flex:1,
		marginTop: STATUS_BAR_HEIGHT,
	},
	headerContainer: {
		marginVertical: 6,
		marginHorizontal: 12,
		padding: 10,
		borderRadius: 10,
		flexDirection: 'row',
		justifyContent: 'space-between',
	},
	headerButton: {
		width: 50,
		alignItems: 'center',
		justifyContent: 'center',
	},
	headerTitle: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center',
	},
	headerText: {
		fontSize: (2* defaultFontSize|0),
		color: color.white,
		// fontFamily: defaultFontFamily,
		textAlign: 'center',
	},
	headerSubText: {
		fontSize: (0.9* defaultFontSize|0),
		color: color.white,
		// fontFamily: defaultFontFamily,
		textAlign: 'center',
	},
	grayContainer: {
		backgroundColor: color.gray,
		marginVertical: 6,
		marginHorizontal: 18,
		paddingVertical: 20,
		paddingHorizontal: 5,
		borderRadius: 10,
	},
	transparentContainer: {
		marginVertical: 6,
		marginHorizontal: 18,
		paddingHorizontal: 5,
		borderRadius: 10,
		flexDirection: 'row',
	},
	temperatureContainer: {
		flex: 3,
	},
	weatherContainer: {
		flex: 2,
		justifyContent: 'center',
		alignItems: 'center',
	},
	statsPanel: {
		flexDirection:'row',
		justifyContent: 'space-evenly',
		paddingVertical: 20,
	},
	statsContainer: {
		marginHorizontal: 15,
	},
	cellContainer: {
		width: 80,
		marginHorizontal: 5,
	},
	cellDate: {
		marginVertical: 15,
		justifyContent:'center',
		marginRight:0,
	},
	forecastContainer: {
		marginHorizontal: 10,
		flexDirection: 'column',
		justifyContent: 'space-between',
		alignItems: 'flex-start',
	},
	forecastItem: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center',
	},
	form: {
		paddingHorizontal: 30,
		paddingVertical: 30,
	},
	formLabel: {
		fontSize: (1.2* defaultFontSize|0),
		color: color.white,
		// fontFamily: defaultFontFamily,
		marginVertical: 4,
	},
	inputField:{
		width: 300,
		backgroundColor: color.grey,
		borderWidth:0,
		paddingVertical:7,
		paddingHorizontal:10,
		marginVertical: 4,
		borderWidth: 1,
		borderColor: color.mistyBlue,
	},
	button: {
		alignItems:'center',
		padding:10,
		paddingHorizontal:50,
		// borderRadius: 5,
		borderWidth: 2,
		fontWeight: 'bold',
		borderColor: color.mistyBlue,
		backgroundColor: color.grey,
	},

	/*IMAGE*/
	iconBig: {
		width: 80,
		height: 70,
	},
	iconMedium: {
		height: 40,
	},

	/*TEXT*/
	center: {
		textAlign: 'center',
	},
	defaultText:{
		fontSize: defaultFontSize,
		color: color.white,
		// fontFamily: defaultFontFamily,
	},
	title1: {
		fontSize: (2* defaultFontSize|0),
		color: color.white,
		// fontFamily: defaultFontFamily,
	},
	title2: {
		fontSize: (1.2* defaultFontSize|0),
		color: color.white,
		// fontFamily: defaultFontFamily,
	},
	temperatureText: {
		color: color.white,
		fontSize: (1.6* defaultFontSize|0),
		textAlign: 'left',
	},
	temperatureText2: {
		fontSize: (3.2* defaultFontSize|0),
		color: color.white,
	},
	errorText: {
		backgroundColor: color.error,
		marginVertical: 4,
		padding: 5,
		flexDirection: 'row',
		width: 300,
		alignItems: 'center',
	}
});


export default Style;