import Axios from "axios"

const APIKey = '436fb1ccb6b7c74cfd07f8641486a653';
const axios = Axios.create({
	baseURL: 'https://api.openweathermap.org/data/2.5',
	// timeout: 30000,
})

export const getWeather = ({lat, lon}) =>(
	// console.log(`/weather?units=metric&mode=JSON&lang=en&lat=${lat}&lon=${lon}&APPID=${APIKey}`) || 
	axios.get(`/weather?units=metric&mode=JSON&lang=en&lat=${lat}&lon=${lon}&APPID=${APIKey}`)
);
export const getForecast = ({lat, lon}) =>(
	axios.get(`/forecast?units=metric&mode=JSON&lang=en&lat=${lat}&lon=${lon}&APPID=${APIKey}`)
);
export default axios