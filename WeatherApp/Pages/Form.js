import { StatusBar } from 'expo-status-bar';
import React, {useEffect, useState, useRef} from 'react'
import { StyleSheet, View, ImageBackground, SafeAreaView, TextInput, Button } from 'react-native';
import SelectDropdown from 'react-native-select-dropdown'

import Style, {BACKGROUND_IMAGE, El, xColor} from '../Style';
import Location from '../Data/koordinat.json';

const Text = El.text;
export default function FormScreen({route, navigation}) {
	const [name, setName] = useState('');
	const [province, setProvince] = useState(null);
	const [city, setCity] = useState(null);
	const [placeholderCity, setPlaceholderCity] = useState('Silahkan pilih provinsi');
	const inputCity = useRef({});
	const arrProvince = Object.keys(Location).sort();
	const [arrCity, setArrCity] = useState([]);
	const [errorText, setErrorText] = useState('');

	/**
	* Set text peringatan
	* jika text kosong, maka peringatan akan disembunyikan
	*/
	const warningCallback = (text='') =>{
		setErrorText(text);
	};

	const onSubmit =()=>{
		if(!name || name.length==0)return warningCallback("Harap isi nama.");
		if(name.length>20)return warningCallback("Nama terlalu panjang.");
		if(!province)return warningCallback("Harap pilih Provinsi.");
		if(!city)return warningCallback("Harap pilih Kabupaten/Kota.");

		warningCallback("");
		navigation.navigate('WeatherInfoScreen',{
			latitude: city.lat,
			longitude: city.lon,
			location: city.name,
			warningCallback,
			name
		});
	};

	useEffect(()=>{
		inputCity.current.reset();
		setCity(null);
		if(province){
			setPlaceholderCity('Pilih Kabupaten/Kota');
			setArrCity((Location[province] || []).sort((a,b)=>a.name>b.name?1:-1));
		}else{
			setPlaceholderCity('Silahkan pilih provinsi');
		}
	},[province]);


	return (
		<ImageBackground style={Style.outerContainer} resizeMode="cover" source={BACKGROUND_IMAGE}>
			<SafeAreaView style={[Style.appContainer, Style.centeredView]}>

				<View style={[Style.grayContainer, Style.form]}>
					<Text style={Style.headerText}>Cari Lokasi</Text>

					<El.error text={errorText} />
					<View  style={Style.inputField}>
						<TextInput 
							style={Style.defaultText}
							onChangeText={v=>setName((v || '').trim())}
							placeholder='Nama Pengguna' 
							placeholderTextColor={xColor.mistyBlue} 
						/>
					</View>

					<SelectDropdown
						buttonStyle={Style.inputField}
						buttonTextStyle={Style.defaultText}
						defaultButtonText='Pilih Provinsi'
						data={arrProvince}
						onSelect={(selectedItem, index) => {
							setProvince(selectedItem);
						}}
						buttonTextAfterSelection={(selectedItem, index) => {
							return selectedItem
						}}
						rowTextForSelection={(item, index) => {
							return item
						}}

						renderCustomizedRowChild={(item, index) => {
							return (
								<Text style={{padding:10, color:xColor.grey}}>{item}</Text>
							)
						}}

					/>

					<SelectDropdown
						buttonStyle={[Style.inputField, province?{}:{backgroundColor:xColor.mistyBlue} ]}
						buttonTextStyle={Style.defaultText}
						defaultButtonText={placeholderCity}
						data={arrCity}
						ref={inputCity}
						disabled={province?false:true}
						onSelect={(selectedItem, index) => {
							setCity(selectedItem);
						}}
						buttonTextAfterSelection={(selectedItem, index) => {
							return selectedItem.name
						}}
						rowTextForSelection={(item, index) => {
							return item.name.replace(/Administrasi/,'');
						}}

						renderCustomizedRowChild={(item, index) => {
							return (
								<Text style={{padding:10, color:xColor.grey}}>{item}</Text>
							)
						}}

					/>


					<El.space />
					<El.button text="LIHAT" onPress={onSubmit} />
				</View>

			</SafeAreaView>
		</ImageBackground>

	);
}

