import { StatusBar } from 'expo-status-bar';
import React, {useEffect, useState} from 'react'
import { View, ImageBackground, SafeAreaView, ScrollView, TouchableOpacity } from 'react-native';
import { MaterialCommunityIcons, Ionicons } from '@expo/vector-icons'; 
import axios from 'axios';

import Style, {BACKGROUND_IMAGE, El} from '../Style';
import {getWeather, getForecast} from '../WeatherAPI';

const Text = El.text;
export default function WeatherInfoScreen({route, navigation}) {
	const {name, latitude, longitude, location, warningCallback} = route.params;
	// console.log("request ",{name, latitude, longitude});


	const greeting = ()=>{
		let now = new Date();
		let time = now.getHours()*60 + now.getMinutes();

		// time >= 04.00 && time < 10.00
		if(time>=240 && time <600){
			return 'Pagi';
		}
		// time >= 10.00 && time < 14.00
		if(time >=600 && time <840){
			return 'Siang';
		}
		// time >= 14.00 && time < 18.30
		if(time >=840 && time <1110){
			return 'Sore';
		}
		return 'Malam';
	};

	const initWeather = {
		temp : ' ',
		tempUnit :' ',
		weatherText: '',
		icon: {uri: "data:image/png;base64, wolQTkcNChoKAAAADUlIRFIAAAABAAAAAQgGAAAAHxXDhMKJAAAACklEQVR4AWMAAQAABQABNsOQwojDnQAAAABJRU5Ewq5CYMKC"},
		humidity:null,
		pressure:null,
		cloudiness: null,
		wind: null,
		date: '',
		greeting: greeting(),
	};
	const dayName = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
	const monthName = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
	const defaultErrorView = (
		<View style={Style.centeredView}>
			<Text style={Style.title2}>Gagal memuat data.</Text>
			<El.space />
			<El.button text="MUAT ULANG" onPress={()=>onReload() } />
		</View>
	);

	const [weather, setWeather] = useState(initWeather);
	const [forecast, setForecast] = useState({});
	const [forecastView, setForecastView] = useState([]);
	const [forecastViewLabel, setForecastViewLabel] = useState([]);
	const [errorView, setErrorView] = useState(null);

	/**
	* mereset ke tampilan saat data weather & forecast belum diambil
	*/
	const resetScreen = () =>{
		setWeather({
			...initWeather,
			greeting: greeting(),
		});
	};


	/**
	* Menghitung waktu pada timezone tertentu
	* @param int epoch : milisecond epoch UTF/GMT+0
	* @param int timezone : selisih menit dari GMT+0
	* @return Date waktu di timezone terpilih, (belum ditest pakai DST)
	*/
	const getDate = (epoch, timezone)=>{
		let date = new Date(epoch);
		date.setSeconds(timezone + date.getTimezoneOffset()*60 );
		return date;
	};

	const getWeatherData = ()=>{
		getWeather({"lat": latitude,"lon": longitude}).then(res=>{
			if(res.data.cod != 200)throw res;
			console.log("> Get weather success");
			// console.log(res.data);
			let date = getDate(res.data.dt * 1000, res.data.timezone);
			let gmt = (res.data.timezone>0?'+':'')+(res.data.timezone/3600);
			let dateFormat = `${dayName[date.getDay()]}, ${monthName[date.getMonth()]} ${date.getDate()}, ${date.getFullYear()} (GMT${gmt})`;
			setWeather({
				temp: res.data.main.temp,
				tempUnit: '\u00B0C',
				weatherText: res.data.weather[0].main,
				icon: {uri:`https://openweathermap.org/img/w/${res.data.weather[0].icon}.png`},
				humidity: res.data.main.humidity,
				pressure: res.data.main.pressure,
				cloudiness: res.data.clouds.all,
				wind: res.data.wind.speed,
				date : dateFormat,
				greeting: greeting(),
			});
		})
		.catch(rej=>{
			console.log("> Get weather fail");
			console.log(rej.data);
			setErrorView(defaultErrorView);
		});
	};

	const getForecastData = ()=>{
		getForecast({"lat": latitude,"lon": longitude}).then(res=>{
			if(res.data.cod != 200)throw res;
			console.log("> Get forecast success");
			let tempForecast = {};
			let arrHour = [];
			let sunrise = getDate(res.data.city.sunrise*1000, res.data.city.timezone);
			let sunset = getDate(res.data.city.sunset*1000, res.data.city.timezone);

			res.data.list.forEach(item=>{
				let date = getDate(item.dt*1000, res.data.city.timezone);
				let index = (date.getMonth()+1)*100 + date.getDate();
				let hour = date.getHours()*100 + date.getMinutes();

				// log jam-jam yag ada di forecast di tiap harinya
				if(arrHour.indexOf(hour) == -1){
					arrHour.push(hour);
				}

				if(!tempForecast[index]){
					// buat array untuk tanggal tertentu
					tempForecast[index]=[];
				}

				// rewrite icon (siang/malam di icon dari API kadang tidak sesuai)
				let dayNight = 'd'; // default day
				if(hour<sunrise.getHours()*100 + sunrise.getMinutes() || hour>=sunset.getHours()*100 + sunset.getMinutes()){
					dayNight = 'n'; // set night
				}


				tempForecast[index].push({
					time :hour,
					icon :item.weather[0].icon.slice(0,-1)+ dayNight, // edit day/night
					weatherText :item.weather[0].main,
					temp :item.main.temp,
					day :date.getDay(),
					show:1,
				});

			});

			// tambah tampilan jika tidak ada data di jam tertentu
			Object.keys(tempForecast).forEach(key=>{
				let hourAvailable = tempForecast[key].map(e=>e.time); // array jam ada data forecast
				arrHour.forEach(hour=>{
					if(hourAvailable.indexOf(hour)==-1){
						// tidak ditemukan data forecast di jam {hour}
						tempForecast[key].push({
							time :hour,
							icon :'',
							weatherText :'',
							temp :0,
							day :0,
							show:0,
						});
					}
				});
			});

			// Test jika terjadi error
			// if(Math.random()>.5) throw '';

			setForecast(tempForecast);

		}).catch(rej=>{
			console.log("> Get forecast fail");
			console.log(rej.data);
			setErrorView(defaultErrorView);
		});
	};



	useEffect(()=>{
		// data requst salah
		if(!name || (!latitude && latitude!==0) || (!longitude && longitude!==0) || !location ){
			(warningCallback || (()=>null))('Data tidak valid');
			navigation.goBack();
		}else{
			getWeatherData();
			getForecastData();
		}

	},[]);

	useEffect(()=>{
		if(Object.keys(forecast).length==0)return;
		let temp = [], label=[];
		Object.keys(forecast).sort((a,b)=>parseInt(a)<parseInt(b)?-1:1).forEach(key=>{
			let item = forecast[key];
			let daySort = dayName[item[0].day].substr(0,3);
			let date = `${key/100|0}/${key%100}`;

			// kolom statis hari/tanggal
			label.push(
				<El.cellDate key={key} day={daySort} date={date} />
			);

			let row = (
				<View key={key} style={{flexDirection:'row'}}>

					{/*Tampilan hari/tanggal di scrollable view*/}
					{/*Tidak perlu dipakai jika sudah ada kolom hari/tanggal statis*/}
					{/*<El.cellDate day={daySort} date={date} />*/}

					{item.sort((a,b)=>a.time<b.time?-1:1).map((e, i)=>(<El.cell key={i} show={e.show} icon={e.icon} time={`${('0'+(e.time/100|0)).substr(-2)}.${('0'+(e.time%100)).substr(-2)}`} value={e.temp + "\u00B0C"} />))}
				</View>
			);
			temp.push(row);
		});
		setForecastView(temp);
		setForecastViewLabel(label);
	},[forecast]);

	// tombol reload ditekan
	const onReload = () =>{
		setErrorView(null);
		resetScreen();
		setForecastView([]);

		getWeatherData();
		getForecastData();

	};

	const onGoBack = ()=>{
		(warningCallback || (()=>null))('');
		return navigation.goBack();
	};

	return (
		<ImageBackground style={Style.outerContainer} resizeMode="cover" source={BACKGROUND_IMAGE}>
			<SafeAreaView style={Style.appContainer}>

				{/*HEADER START*/}
				<View style={Style.headerContainer}>
					<TouchableOpacity  style={Style.headerButton} onPress={onGoBack}>
						<Text><Ionicons name="arrow-back-outline" size={34} /></Text>
					</TouchableOpacity>

					<View  style={Style.headerTitle}>
						{/* Test Text terpanjang <Text style={Style.headerText}>Kabupaten Kepulauan Siau Tagulandang Biaro</Text>*/}
						<Text style={Style.headerText}>{location}</Text>
						<Text style={Style.headerSubText}>{weather.date}</Text>
					</View>

					<TouchableOpacity  style={Style.headerButton} onPress={()=>onReload()}>
						<Text><MaterialCommunityIcons name="reload" size={34} /></Text>
					</TouchableOpacity>

				</View>
				{/*HEADER END*/}

				{/*WEATHER PANEL START*/}
				<View style={Style.transparentContainer}>
					<View style={Style.temperatureContainer}>
						<Text style={Style.title2}>Selamat {weather.greeting}, {name}</Text>
						<Text style={Style.temperatureText}>
							<Text style={Style.temperatureText2}>{weather.temp}</Text>
							&nbsp;{weather.tempUnit}
						</Text>
					</View>
					<View style={Style.weatherContainer}>
						<Text style={[Style.title2,{textAlign:'center'}]}>{weather.weatherText}</Text>
						<ImageBackground style={Style.iconBig} source={weather.icon} resizeMode='contain'>
						</ImageBackground>
					</View>
				</View>

				<View style={[Style.grayContainer, Style.statsPanel]}>
					{forecastView.length>0?(<>
						<El.statsIcon show={weather.humidity!=null?1:0} title='Humidity' value={weather.humidity + " %"} icon={(<MaterialCommunityIcons name="water-percent" size={34} />)} />
						<El.statsIcon show={weather.pressure!=null?1:0} title='Pressure' value={weather.pressure + " hPa"} icon={(<MaterialCommunityIcons name="speedometer" size={34} />)} />
						<El.statsIcon show={weather.cloudiness!=null?1:0} title='Cloudiness' value={weather.cloudiness + " %"} icon={(<MaterialCommunityIcons name="cloud" size={34} />)} />
						<El.statsIcon show={weather.wind!=null?1:0} title='Wind' value={weather.wind + " m/s"} icon={(<MaterialCommunityIcons name="weather-windy" size={34} />)} />
					</>):(<>
						<El.statsIcon show={0} title='' value='' icon='' />
					</>)

					}
				</View>

				{/*WEATHER PANEL END*/}


				{/*FORECAST PANEL START*/}
				<View style={[Style.grayContainer, {flex:1, flexDirection:'row'}]}>
					{!errorView && forecastView.length>0?(<>
						<View style={Style.forecastContainer}>
							{forecastViewLabel}
						</View>
						<ScrollView horizontal>
							<View style={Style.forecastContainer}>
								{forecastView}
							</View>
						</ScrollView>
					</>):(
						errorView || (<View style={Style.centeredView}><Text style={Style.center}><Ionicons name="hourglass-outline" size={44} /></Text></View>)
					)}
				</View>
				{/*FORECAST PANEL END*/}

			</SafeAreaView>
		</ImageBackground>
	);
}

