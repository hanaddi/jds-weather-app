import { StatusBar } from 'expo-status-bar';
import AppLoading from 'expo-app-loading';
import React, {useEffect} from 'react'
import { View, Text, Image, StyleSheet, SafeAreaView, FlatList,TouchableOpacity, Button } from 'react-native'
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { MaterialCommunityIcons } from '@expo/vector-icons'; 

import FormScreen from './Pages/Form';
import WeatherInfoScreen from './Pages/WeatherInfo';

const Stack = createStackNavigator();
const Manager = (param) =>{
	console.log("------------------------- App -------------------------");

	return (
		<>
		<NavigationContainer>
			<Stack.Navigator initialRouteName="FormScreen" screenOptions={{unmountOnBlur: true, headerShown: false}}>
				<Stack.Screen name="FormScreen" component={FormScreen}  />
				{<Stack.Screen name="WeatherInfoScreen" component={WeatherInfoScreen} />}
			</Stack.Navigator>
		</NavigationContainer>
		</>
	);
};

export default function Router (){
	return (
		<Manager />
	);
};
