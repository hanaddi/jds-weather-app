# JDS Weather App
Aplikasi untuk menampilkan prakiraan cuaca yang diambil dari Open Weather Map


#### Informasi
- Nama peserta  :Fat Han Nuraddin
- Kelas :React Native
- Deploy url :https://exp-shell-app-assets.s3.us-west-1.amazonaws.com/android/%40hanaddi/WeatherApp-ce7576f4d4c14170b591b4399e06c991-signed.apk
- Video demo : https://youtu.be/Obyxerqycfw


#### Sumber gambar
- [Photo](https://www.pexels.com/photo/brown-mountains-1643113/) by [Aron](https://www.instagram.com/aronvisuals) Visuals from [Pexels](https://www.pexels.com)
- [Photo](https://www.pexels.com/photo/snow-dawn-landscape-sunset-4991261/) by [Brilian Yoga](https://instagram.com/brilian_yoga) Visuals from [Pexels](https://www.pexels.com)


#### Sumber data
- Koordinat Kabupaten/Kota :https://datahub.io/JemputData/location_id
- Koordinat Kabupaten/Kota Administratif (di DKI Jakarta) :https://www.google.com/maps
- Cuaca dan prakiraan cuaca :https://openweathermap.org/